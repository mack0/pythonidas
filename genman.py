from __future__ import print_function
import frams
import sys

def main(argv):

	frams.init()

	genman = frams.GenMan()

	items = 30

	geno_list = [ genman.getSimplest('4') ]
	model_list = []

	for i in range(items-1):
		geno_list.append(genman.mutate(geno_list[-1]))

	for i in range(items):
		model_list.append(frams.Model(geno_list[i]))

	for i in range(items):
		print("{}: {}".format(i, model_list[i].getPartCount()))

if __name__ == '__main__':
	exit(main(sys.argv))
