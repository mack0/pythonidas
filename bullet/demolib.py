
import time
import math
import numpy as np

import pygame.display

from OpenGL.GL import (
    GL_TRIANGLE_STRIP, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT,
    glPushMatrix, glPopMatrix, glColor, glClear,
    glBegin, glEnd, glTranslate, glRotate, glVertex)
from OpenGL.GLU import gluNewQuadric, gluSphere, gluCylinder

from ctypes import CDLL
bullet_so = 'bullet.so' # https://github.com/dgym/pybullet
bullet = CDLL(bullet_so)

from bullet import Vector3, Transform, BoxShape, SphereShape, CylinderShape, DefaultMotionState, RigidBody


class Ground:
    def __init__(self):
        self.boxHalfExtents = Vector3(5, 1, 5)
        groundShape = BoxShape(self.boxHalfExtents)
        groundTransform = Transform()
        groundTransform.setIdentity()
        groundTransform.setOrigin(Vector3(0, -4, 0))
        groundMotion = DefaultMotionState()
        groundMotion.setWorldTransform(groundTransform)
        self.body = RigidBody(groundMotion, groundShape)
        self.body.setRestitution(0.5)
        self.motion = groundMotion

    def render(self):
        x, y, z = (
            self.boxHalfExtents.x, self.boxHalfExtents.y, self.boxHalfExtents.z)
        o = self.motion.getWorldTransform().getOrigin()
        glColor(0, 0, 255)
        glTranslate(o.x, o.y, o.z)
        glBegin(GL_TRIANGLE_STRIP)
        glVertex(-x, y, -z)
        glVertex(x, y, -z)
        glVertex(-x, y, z)
        glVertex(x, y, z)
        glEnd()

class Sphere:
    def __init__(self, quad, position, color, radius):
        self.quad = quad
        self.radius = radius
        self.shape = SphereShape(self.radius)
        self.transform = Transform()
        self.transform.setIdentity()
        self.transform.setOrigin(position)
        sphereMotion = DefaultMotionState()
        sphereMotion.setWorldTransform(self.transform)
        self.body = RigidBody(sphereMotion, self.shape, self.radius)
        self.body.setRestitution(0.0)
        self.motion = sphereMotion
        self.color = color

    def render(self):
        o = self.motion.getWorldTransform().getOrigin()
        glColor(*self.color)
        glTranslate(o.x, o.y, o.z)
        gluSphere(self.quad, self.radius, 25, 25)

class Cylinder:
    def __init__(self, quad, position1, position2, color, radius):
        self.quad = quad
        length = math.sqrt(sum([(position2[i] - position1[i])**2 for i in range(3) ]))
        self.dimensions = Vector3(radius, radius, abs(length))
        self.position1 = position1
        self.position2 = position2

        self.shape = CylinderShape(self.dimensions)
        self.transform = Transform()
        self.transform.setIdentity()
        self.transform.setOrigin(position1)
        cylinderMotion = DefaultMotionState()
        cylinderMotion.setWorldTransform(self.transform)
        self.body = RigidBody(cylinderMotion, self.shape, 2.0)
        self.body.setRestitution(0.0)
        self.motion = cylinderMotion
        self.color = color

    def render(self):
        o = self.motion.getWorldTransform().getOrigin()
        glColor(*self.color)

        vx = self.position2[0] - self.position1[0] #x2-x1
        vy = self.position2[1] - self.position1[1] #y2-y1
        vz = self.position2[2] - self.position1[2] #z2-z1
        if vz == 0: vz = 0.00001;

        v = math.sqrt(vx*vx + vy*vy + vz*vz)
        ax = 57.2957795*math.acos(vz/v)
        if vz < 0.0: ax = -ax
        rx = -vy*vz;
        ry = vx*vz;

        glTranslate(o.x, o.y, o.z)
        glRotate(ax, rx, ry, 0.0);
        gluCylinder(self.quad, self.dimensions[0], self.dimensions[1], self.dimensions[2], 25, 25)

def step(world):
    timeStep = fixedTimeStep = 1.0 / 60.0
    world.stepSimulation(timeStep, 1, fixedTimeStep)
    now = time.time()
    delay = now % timeStep
    time.sleep(delay)

def render(objects):
    glPushMatrix()
    for o in objects:
        glPushMatrix()
        o.render()
        glPopMatrix()
    glPopMatrix()
    #glRotate(0.3, 0, 1, 0)

    pygame.display.flip()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

def simulate(world, objects):
    while True:
        step(world)
        render(objects)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    glRotate(-30, 0, 1, 0)
                elif event.key == pygame.K_RIGHT:
                    glRotate(30, 0, 1, 0)
                elif event.key == pygame.K_DOWN:
                    glRotate(-30, 1, 0, 0)
                elif event.key == pygame.K_UP:
                    glRotate(30, 1, 0, 0)

