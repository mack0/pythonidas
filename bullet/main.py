'''
An attempt to draw a sample collision shape consisting of 2 spheres and a cylinder.
Encountered problem: compound of collision solids.
'''

import pygame.locals, pygame.display

from OpenGL.GL import (
    GL_DEPTH_TEST, glEnable, glTranslate)
from OpenGL.GLU import gluPerspective

from ctypes import CDLL
bullet_so = 'bullet.so' # https://github.com/dgym/pybullet
bullet = CDLL(bullet_so)

from bullet import *
from demolib import * 

def main():
    pygame.init()
    pygame.display.set_mode(
        (1024, 768), pygame.locals.OPENGL | pygame.locals.DOUBLEBUF)

    glEnable(GL_DEPTH_TEST)
    gluPerspective(60.0, 640.0 / 480.0, 0.5, 1000.0)
    glTranslate(0, -15, -60)

    dynamicsWorld = DiscreteDynamicsWorld()
    dynamicsWorld.setGravity(Vector3(0, -9.81, 0));

    quad = gluNewQuadric()

    spheres = []
    cylinders = []
    
    spheres.append(Sphere(quad, Vector3(10, 5, 0), (255, 0, 0), 2))
    spheres.append(Sphere(quad, Vector3(20, 15, 0), (255, 0, 0), 2))
    cylinders.append(Cylinder(quad, Vector3(10, 5, 0), Vector3(20, 15, 0), (255, 0, 0), 2))
    cylinders.append(Cylinder(quad, Vector3(20, 15, 0), Vector3(10, 5, 0), (255, 0, 0), 2))

    figureTransform = Transform()
    figureTransform.setIdentity()
    figureTransform.setOrigin(Vector3(0, 5, 0))

    figure = CompoundShape()
    for o in spheres + cylinders:
        figure.addChildShape(figureTransform, o.shape)

    #ground = Ground()
    #dynamicsWorld.addRigidBody(ground.body)
    for o in spheres + cylinders:
        dynamicsWorld.addRigidBody(o.body)

    simulate(dynamicsWorld, spheres + cylinders) # + [ground])
 

if __name__ == '__main__':
    main()
