import time
from OpenGL.GL import *
from OpenGL.GLU import gluNewQuadric, gluLookAt, gluPerspective
from pyopengltk import OpenGLFrame
from genotype_to_model import create_objects
from genotype_error import GenotypeError

CAMERA_LEFT = 0
CAMERA_RIGHT = 1
CAMERA_UP = 2
CAMERA_DOWN = 3
CAMERA_FORWARD = 4
CAMERA_BACKWARD = 5
CAMERA_ZERO = 6

class AppOpenGL(OpenGLFrame):
    def __init__(self, *args, **kw):
        self.print_fps = False # controls displaying fps to stdout
        self.width = kw['width'] if 'width' in kw else 500
        self.height = kw['height'] if 'height' in kw else 500
        if 'print_fps' in kw:
            self.print_fps = kw['print_fps']
            self.fps_s = 0
            self.n_frames = 0
            del kw['print_fps']

        self.objects = []
        self.orig_translate = [0, 0, 0]
        self.translate = [*self.orig_translate] # objects group position
        self.translation_step = (0.2, 0.1, .1) # moving objects speed (3 axis)
        self.rotation = [0.0, 0.0, 0.0, 1.0]

        # some parameters for models' creation
        self.start_x = .0
        self.max_height = .0
        self.distance = 2
        self.stick_radius = .1

        self.display_models = True
        self.on_draw_hooks = []
        OpenGLFrame.__init__(self, *args, **kw)

    def initialize_new_genotype(self, genotype):
        if genotype.model is None:
            raise GenotypeError('Cannot create 3D object: model is None.')
        if not genotype.is_valid():
            raise GenotypeError('Cannot create 3D object: genotype is not valid.')

        # TODO should be changed when many genotype support is implemented
        models = [genotype.model]
        self.objects, self.orig_translate = create_objects(
            models,
            self.translate,
            start_x = self.start_x,
            max_height = self.max_height,
            distance = self.distance,
            stick_radius = self.stick_radius
        )
        self.translate = [*self.orig_translate]

    def add_on_draw_hook(self, function):
        '''
        Adds function to a list with functions that are called every time openGL models are being refreshed.
        '''
        if function not in self.on_draw_hooks:
            self.on_draw_hooks.append(function)

    def clear(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        glMatrixMode(GL_MODELVIEW)

    def start_displaying(self):
        self.display_models = True

    def stop_displaying(self):
        self.clear()
        self.display_models = False

    def on_draw(self):
        self.clear()

        if self.display_models:
            for o in self.objects:
                glPushMatrix()
                glTranslatef(*self.translate)
                glRotatef(65, 1, 0, 0)
                glRotatef(180, 0, 0, 1)
                o.draw(self.rotation)
                glPopMatrix()

    def rotate(self, dt):
        self.rotation[0] += dt * 30
        self.rotation[0] %= 360

    def change_camera_position(self, position):
        if position == CAMERA_LEFT:
            self.translate[0] -= self.translation_step[0]
        elif position == CAMERA_RIGHT:
            self.translate[0] += self.translation_step[0]
        elif position == CAMERA_DOWN:
            self.translate[1] -= self.translation_step[1]
        elif position == CAMERA_UP:
            self.translate[1] += self.translation_step[1]
        elif position == CAMERA_BACKWARD:
            self.translate[2] += self.translation_step[2]
        elif position == CAMERA_FORWARD:
            self.translate[2] -= self.translation_step[2]
        elif position == CAMERA_ZERO:
            self.translate = [*self.orig_translate]

    def initgl(self):
        """Initalize gl states when the frame is created or when the window is resized"""
        glViewport(0, 0, self.width, self.height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(-90., self.width / float(self.height), .1, 1000.)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        glClearColor(0, 0, 0, 1)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)

        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)

        glEnable(GL_NORMALIZE)
        glEnable(GL_COLOR_MATERIAL)

        if self.print_fps:
            self.fps_s = time.time()

    def redraw(self):
        s = time.time()
        """Render a single frame"""
        self.on_draw()

        # call all functions that should be invoked on redraw
        for fun in self.on_draw_hooks:
            if callable(fun):
                fun()
        dt = time.time()-s
        self.rotate(dt)

        if self.print_fps:
            self.n_frames += 1
            print("fps",self.n_frames / (time.time() - self.fps_s), end="\r" )