#!/usr/bin/env python

from __future__ import print_function

from pyglet.gl import *
from pyglet.window import key, mouse
import sys
import math
import argparse

import frams

from cuboid import Cuboid
from cylinder import Cylinder
from sphere import Sphere
from stick import Stick

objects = []
stick_radius = 0.1

transtale = [0, 0, 0] # objects group position
translation_step = (0.5, 0.3, 1.0) # moving objects speed (3 axis)
rotation = [0.0, 0.0, 0.0, 1.0]
rotation_step = 30

try:
    config = Config(sample_buffers=1, samples=4,
        depth_size=16, double_buffer=True,)
    window = pyglet.window.Window(resizable=True, config=config, caption='pythonidas')
except pyglet.window.NoSuchConfigException:
    window = pyglet.window.Window(resizable=True)

@window.event
def on_key_press(symbol, modifiers):
    global translation_step, transtale

    if symbol == key.LEFT:
        transtale[0] += translation_step[0]
    elif symbol == key.RIGHT:
        transtale[0] -= translation_step[0]

    elif symbol == key.UP:
        transtale[1] -= translation_step[1]
    elif symbol == key.DOWN:
        transtale[1] += translation_step[1]

    elif symbol == key.PLUS or symbol == key.EQUAL:
        transtale[2] += translation_step[2]
    elif symbol == key.MINUS:
        transtale[2] -= translation_step[2]

    elif symbol == key.F5:
        window.set_fullscreen(True)
    elif symbol == key.ESCAPE:
        window.set_fullscreen(False)
        return pyglet.event.EVENT_HANDLED

@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(-90., width / float(height), .1, 1000.)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    return pyglet.event.EVENT_HANDLED

def update(dt):
    global rotation
    rotation[0] += dt * 30
    rotation[0] %= 360
pyglet.clock.schedule(update)

@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glMatrixMode(GL_MODELVIEW)

    glPushMatrix()
    glTranslatef(transtale[0], transtale[1], transtale[2])
    glRotatef(90, 1, 0, 0)
    glRotatef(180, 0, 0, 1)
    for o in objects:
        glPushMatrix()
        o.draw(rotation)
        glPopMatrix()
    glPopMatrix()

def init_opengl():
    glClearColor(0, 0, 0, 1)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)

    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)

    glEnable(GL_NORMALIZE)
    glEnable(GL_COLOR_MATERIAL)

def get_dimensions(m):
    min_x, min_y, min_z = m.getPart(0).p.x, m.getPart(0).p.y, m.getPart(0).p.z
    max_x, max_y, max_z = min_x, min_y, min_z

    for i in range(m.getPartCount()):
        x, y, z = m.getPart(i).p.x, m.getPart(i).p.y, m.getPart(i).p.z
        if x < min_x: min_x = x
        elif x > max_x: max_x = x
        if y < min_y: min_y = y
        elif y > max_y: max_y = y
        if z < min_z: min_z = z
        elif z > max_z: max_z = z
    return (min_x, min_y, min_z, max_x, max_y, max_z)

def create_parts(glQuadratic, m, translate_to_position, translate_center):
    parts = []
    for i in range(m.getPartCount()):
        if m.getPart(i).shape == frams.Part.Shape.SHAPE_BALL_AND_STICK:
            parts.append(Sphere(glQuadratic, stick_radius, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_ELLIPSOID:
            parts.append(Sphere(glQuadratic, 1, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_CUBOID:
            parts.append(Cuboid(glQuadratic, 1, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_CYLINDER:
            parts.append(Cylinder(glQuadratic, 1, 2, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        else:
            raise Exception("Unknown part shape: '" + str(m.getPart(i).shape) + "'")
    return parts

def create_joints(glQuadratic, m, parts, translate_to_position, translate_center):
    joints = []
    for i in range(m.getJointCount()):
        if m.getJoint(i).shape == frams.Joint.Shape.SHAPE_BALL_AND_STICK:
            p1, p2 = m.getJoint(i).p1_refno, m.getJoint(i).p2_refno
            joints.append(Stick(glQuadratic, stick_radius, parts[p1].position, parts[p2].position, translate_to_position, translate_center, m.getJoint(i).vcolor ))
        elif m.getJoint(i).shape == frams.Joint.Shape.SHAPE_FIXED:
            pass
        else:
            raise Exception("Unknown joint shape: '" + str(m.getJoint(i).shape) + "'")
    return joints

def create_objects(models):
    glQuadratic = gluNewQuadric()
    start_x = 0.0
    max_height = 0.0
    distance = 2
    for m in models:
        dimensions = get_dimensions(m)

        translate_to_position = (start_x - dimensions[0], -(dimensions[1] + dimensions[4])*1.0/2, -(dimensions[2] + dimensions[5])*1.0/2)
        start_x = start_x + math.sqrt((dimensions[3] - dimensions[0])**2 + (dimensions[4] - dimensions[1])**2) + distance
        translate_center = ((dimensions[3]+dimensions[0])/2, (dimensions[4]+dimensions[1])/2, (dimensions[5]+dimensions[2])/2)

        height = dimensions[4]-dimensions[1]
        if height > max_height: max_height = height

        parts = create_parts(glQuadratic, m, translate_to_position, translate_center)
        joints = create_joints(glQuadratic, m, parts, translate_to_position, translate_center)

        global objects
        objects += parts
        objects += joints

    # move camera
    transtale[0] = (start_x-distance)/2
    transtale[2] = -max(transtale[0], 2.5*max_height)

def main(genotypes):
    models = []
    for genotype in genotypes:
        g = frams.Geno(genotype)
        m = frams.Model(g)
        if m.isValid() == False:
            print("Invalid genotype")
        else:
            models.append(m)

    if len(models) == 0: sys.exit(1)

    init_opengl()
    create_objects(models)
    pyglet.app.run()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    group = parser.add_argument_group('genotype source')
    group.add_argument("--genotype", "-g", nargs="*", help="genotype string")
    group.add_argument("--file", "-f", nargs="*", help="file with genotype")
    args = parser.parse_args()

    if args.genotype == None and args.file == None:
        print("Either --genotype (-g) or --file (-f) argument is required.")
        sys.exit(1)

    frams.init()
    genotypes = []
    if args.file != None:
        for input_file in args.file:
            with open(input_file, 'r') as f:
                loader = frams.GenotypeMiniLoader(input_file)
                l = loader.loadNextGenotype()
                if not l:
                    genotypes.append(f.read())
                else:
                    while l:
                        genotypes.append(l.genotype.c_str())
                        l = loader.loadNextGenotype()
    if args.genotype != None:
        for g in args.genotype:
            genotypes.append(g)

    main(genotypes)
