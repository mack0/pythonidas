#!/usr/bin/env python

from pyglet.gl import *
import pyglet

import math
import numpy as np

class Stick(object):
    def __init__(self, glQuadratic, radius, position1, position2, translate_to_position, translate_to_center, color):
        self.glQuadratic = glQuadratic
        self.radius = radius
        self.position1 = position1
        self.position2 = position2
        self.color = color
        self.translate_to_position = translate_to_position
        self.translate_to_center = translate_to_center

        vx = self.position2.x - self.position1.x
        vy = self.position2.y - self.position1.y
        vz = self.position2.z - self.position1.z

        v = np.array([vx, vy, vz])
        self.length = math.sqrt(np.dot(v,v))

        z = np.array([0.0, 0.0, 1.0])
        if vx == 0 and vy == 0: 
            self.ax = [0.0, 1.0, 0.0]
        else:
            self.ax = np.cross(z, v)
        self.angle = 180.0 / math.pi * math.acos(np.dot(z, v) / self.length)

    def draw(self, rotation):
        glColor3f(self.color.x, self.color.y, self.color.z)
        glPushMatrix()
        
        glTranslatef(self.translate_to_position[0], self.translate_to_position[1], self.translate_to_position[2])
        glTranslatef(self.translate_to_center[0], self.translate_to_center[1], self.translate_to_center[2])
        glRotatef(rotation[0], rotation[1], rotation[2], rotation[3]) 
        glTranslatef(-self.translate_to_center[0], -self.translate_to_center[1], -self.translate_to_center[2])

        glTranslatef(self.position1.x, self.position1.y, self.position1.z)
        glRotatef(self.angle, self.ax[0], self.ax[1], self.ax[2])
        gluCylinder(self.glQuadratic, self.radius, self.radius, self.length, 25, 25)
        glPopMatrix()
        
