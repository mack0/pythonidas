import tkinter as tk
from tkinter import messagebox as msg
from tkinter import ttk
from tkinter import filedialog
import appOpenGL as gl
import utils as u
import os
import genotype_validator as validator
import frams
from simulation import Simulation
from genotype import GenotypeObject
from genotype_error import GenotypeError
from pybullet_wrapper import PybulletWrapper
import argparse

# when enabled then pybullet will open with pythonidas window
# this option should be set to True from command line using --pybullet switch
ENABLE_PYBULLET=False

class FramstickWindow:
    def __init__(self):
        super().__init__()
        self.root = tk.Tk()
        self.genotype = tk.StringVar(self.root)
        self.genotype.trace('w', self.on_genotype_changed)
        self.editor = None
        self.log_console = None
        self.gl_frame = None
        self.simulation = Simulation()
        self.simulation_is_on = tk.BooleanVar(value=False)
        self.simulation_text = tk.StringVar()
        self.update_simulation_state(self.simulation_is_on.get())
        # changing camera movement is forbidden at the startup = editor is active
        self.change_camera_movement = False

        # keeps all main frames in the window
        self.root_frames = dict()
        self.prepare_gui()
        self.init_keyboard_bindings()
        self.init_menu()

        if ENABLE_PYBULLET:
            self.bullet_engine = PybulletWrapper()
            self.init_pybullet()
        else:
            self.bullet_engine = None


    def init_pybullet(self):
        if self.gl_frame is None:
            raise RuntimeError('Open gl frame is not initialized')
        if self.bullet_engine is None:
            self.log('Pybullet is not available', u.INFO)
            return
        self.gl_frame.add_on_draw_hook(self.bullet_engine.on_draw)

    def set_camera(self, position):
        if self.gl_frame is not None and self.change_camera_movement:
            self.gl_frame.change_camera_position(position)

    def init_keyboard_bindings(self):
        self.root.bind("<F9>", self.toggle_simulation)
        self.root.bind("<F8>", self.one_step)
        self.root.bind("<Control-s>", self.save_genotype)
        # TODO add save as Control-Alt-s
        self.root.bind("<Control-o>", self.open_genotype)
        self.root.bind("<F1>", lambda _: self.clear_log())
        self.root.bind("<F2>", lambda _: self.genotype.set(''))

        self.root.bind("<Left>", lambda _: self.set_camera(gl.CAMERA_LEFT))
        self.root.bind("<Right>", lambda _: self.set_camera(gl.CAMERA_RIGHT))
        self.root.bind("<Up>", lambda _: self.set_camera(gl.CAMERA_UP))
        self.root.bind("<Down>", lambda _: self.set_camera(gl.CAMERA_DOWN))

        self.root.bind("-", lambda _: self.set_camera(gl.CAMERA_FORWARD))
        self.root.bind("<Control-Key-equal>", lambda _: self.set_camera(gl.CAMERA_BACKWARD))

        # set initial camera position
        self.root.bind("<Control-Key-0>", lambda x: self.gl_frame.change_camera_position(gl.CAMERA_ZERO))

    def clear_log(self):
        self.log_console.configure(state='normal')
        self.log_console.clear()
        self.log_console.configure(state='disabled')

    def log(self, message, severity):
        '''
        Logs message to a console with specified severity.
        '''
        if self.log_console is None:
            return
        data = f'[{u.get_log_time()}][{severity}]: {message}\n'
        # hack with changing state - to make log console readonly
        # set state to normal to be able to add a new text
        self.log_console.configure(state='normal')
        self.log_console.insert(tk.END, data)
        # disable adding new texts
        self.log_console.configure(state='disabled')
        # scroll to the end
        self.log_console.see('end')

    def display_message(self, message, severity):
        '''
        Displays dialog box with a message and specified severtiy.
        '''
        self.log(message, severity)
        if severity == u.INFO:
            msg.showinfo('Info', message)
        elif severity == u.WARNING:
            msg.showwarning('Warning', message)
        elif severity == u.ERROR:
            msg.showerror('Error', message)

    def on_genotype_changed(self, *args):
        if self.editor is None:
            self.display_message('Editor not found', severity=u.ERROR)

    def update_current_genotype(self):
        '''
        Synchronizes genotype stored in the ScrollableText field
        and simulation.active_genotype.
        '''
        if self.simulation.active_genotype is None:
            # add the default genotype
            self.simulation.add_genotype(GenotypeObject(genotype=self.genotype.get()))
        else:
            self.simulation.active_genotype.set_genotype(self.genotype.get())

    def update_simulation_state(self, new_state):
        '''
        Updates simulation state. Used for starting stopping simulation.
        Updates genotype based on value in the editor, validates it and updates all
        elements in the gui accordingly to the new_state.
        '''

        self.update_current_genotype()
        if new_state and self.genotype.get() == '':
            self.log('No genotype provided - cannot start simulation.', u.WARNING)
            return
        elif new_state and not self.simulation.active_genotype.is_valid():
            self.log('Provided genotype is not valid - cannot start simulation.', u.ERROR)
            return

        if new_state and self.gl_frame is not None:
            try:
                self.gl_frame.initialize_new_genotype(self.simulation.active_genotype)
                self.gl_frame.start_displaying()
                # ensures that focus is not on the editor - to move camera without
                # moving cursor
                self.gl_frame.focus()
            except GenotypeError as e:
                self.display_message(f'OpenGL error: {e}', u.ERROR)
                return
            except Exception as e:
                self.display_message(f'Failed to create 3D objects: {e}', u.ERROR)
                return

        # stop simulation
        if not new_state and self.gl_frame is not None:
            self.gl_frame.stop_displaying()

        self.simulation_is_on.set(new_state)
        self.simulation_text.set('on' if self.simulation_is_on.get() else 'off')
        self.log(f'Simulation {"started" if self.simulation_is_on.get() else "stopped"}', u.INFO)

    def toggle_simulation(self, *args):
        new_state = not self.simulation_is_on.get()
        self.update_simulation_state(new_state)
        self.set_focus_on_editor(not new_state)

    def start_simulation(self, *args):
        self.update_simulation_state(True)
        self.set_focus_on_editor(False)

    def stop_simulation(self, *args):
        self.update_simulation_state(False)
        self.set_focus_on_editor(True)

    def one_step(self, *args):
        if self.simulation_is_on.get():
            self.stop_simulation()
        # TODO add one step
        self.log('one step', u.INFO)

    def open_genotype(self, *args):
        file = filedialog.askopenfilename(title = "Select file with genotype",
            filetypes=(('genotype file', '*.gen'), ('all files', '*.*')))
        if not file:
            return
        validation_result, data = validator.validate_genotype_from_file(file)
        if not validation_result:
            self.display_message(f'Failed to parse and validate genotypes from file: {file}', severity=u.WARNING)
        else:
            self.log(f'Opened genotype file: {file}', u.INFO)
            if len(data) > 0:
                # create genotypes from that model
                self.simulation.remove_all_genotypes()
                for genotype in data:
                    self.simulation.add_genotype(genotype)
                # put first genotype (the active one) into the editor window
                self.genotype.set(self.simulation.active_genotype.genotype)
            else:
                self.display_message(f'Failed to parse any model from file: {file}', severity=u.WARNING)

    def save_genotype(self, *args):
        self.update_current_genotype()
        if self.simulation.active_genotype.genotype == '':
            self.display_message(f'There is nothing to be saved', severity=u.INFO)
            return

        file = filedialog.asksaveasfilename(title = "Save genotype",
            filetypes = (("genotype file","*.gen"),("all files","*.*")))
        if not file:
            return

        if self.simulation.active_genotype.save_genotype(file):
            self.log(f'Saved file: {file}', u.INFO)
        else:
            self.display_message(f'Failed to save: {file}', severity=u.WARNING)

    def set_focus_on_editor(self, set_focus):
        if set_focus and self.editor is not None:
            self.editor.focus()
        elif not set_focus and self.gl_frame is not None:
            self.gl_frame.focus()

        if self.gl_frame is not None:
            self.change_camera_movement = not set_focus

    def visualization_got_clicked(self, evt):
        self.set_focus_on_editor(False)

    def editor_got_clicked(self, evt):
        self.set_focus_on_editor(True)

    def init_menu(self):
        menu = tk.Menu(self.root)
        self.root.config(menu=menu)
        filemenu = tk.Menu(menu)
        menu.add_cascade(label="Genotype", menu=filemenu)
        filemenu.add_command(label='Open genotype', command=self.open_genotype, accelerator="Ctrl+o")
        filemenu.add_command(label='Save genotype', command=self.save_genotype, accelerator="Ctrl+s")
        filemenu.add_separator()
        filemenu.add_command(label='Clear genotype', command=lambda : self.genotype.set(''), accelerator='F2')

        simulation = tk.Menu(menu)
        menu.add_cascade(label="Simulation", menu=simulation)
        simulation.add_command(label='Toggle simulation state', command=self.toggle_simulation, accelerator="F9")
        simulation.add_command(label='Step', command=self.one_step, accelerator="F8")

        log = tk.Menu(menu)
        menu.add_cascade(label="Log", menu=log)
        log.add_command(label='Clear log', command=lambda : self.clear_log(), accelerator="F1")

        camera = tk.Menu(menu)
        menu.add_cascade(label="Camera", menu=camera)
        camera.add_command(label='Up', command=lambda : self.set_camera(gl.CAMERA_UP), accelerator="Up")
        camera.add_command(label='Down', command=lambda : self.set_camera(gl.CAMERA_DOWN), accelerator="Down")
        camera.add_command(label='Left', command=lambda : self.set_camera(gl.CAMERA_LEFT), accelerator="Left")
        camera.add_command(label='Right', command=lambda : self.set_camera(gl.CAMERA_RIGHT), accelerator="Right")
        camera.add_command(label='Forward', command=lambda : self.set_camera(gl.CAMERA_FORWARD), accelerator="Ctrl+=")
        camera.add_command(label='Backward', command=lambda : self.set_camera(gl.CAMERA_BACKWARD), accelerator="Ctrl+-")
        camera.add_command(label='Initial position', command=lambda : self.set_camera(gl.CAMERA_ZERO), accelerator="Ctrl+0")

    def prepare_controls(self):
        # left side
        # openGL canvas
        ttk.Label(self.root_frames['opengl'], text='Simulation state:')\
            .grid(row=0, column=0, sticky='w')
        ttk.Label(self.root_frames['opengl'],
            textvariable=self.simulation_text).grid(row=0, column=0, sticky='e')

        self.gl_frame = gl.AppOpenGL(self.root_frames['opengl'],
            print_fps = False,
            width=700,
            height=700)
        self.gl_frame.grid(row=1, column=0, columnspan=2, sticky=(tk.N, tk.S, tk.E, tk.W))
        self.gl_frame.animate = 1
        # set focus on the openGL frame (remove from editor)
        self.gl_frame.bind("<Button-1>", self.visualization_got_clicked)

        ttk.Label(self.root_frames['log_console'], text='Log window:')\
            .grid(row=0, column=0, sticky='w')
        self.log_console = u.ScrolledText(self.root_frames['log_console'], height=5, state=tk.DISABLED)
        self.log_console.grid(row=1, column=0, columnspan=2, sticky=(tk.N, tk.E, tk.S, tk.W))

        ## right bottom
        ### genotype editor
        ttk.Label(self.root_frames['editor'], text='Genotype editor')\
            .grid(row=0, column=0, columnspan=2, sticky='w')
        self.editor = u.ScrolledText(self.root_frames['editor'], textvariable=self.genotype)
        self.editor.grid(row=1, column=0, columnspan=2, sticky=(tk.N, tk.E, tk.S, tk.W))

        # set focus on the editor initially
        self.editor.focus()
        # set focus on the editor when clicked (and remove from openGL frame)
        self.editor.bind("<Button-1>", self.editor_got_clicked)

    def prepare_gui(self):
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.root.minsize(800, 500)
        self.root.title('Pythonidas')

        # main content frame
        content = ttk.Frame(self.root, padding=(2,2,2,2))
        content.grid(column=0, row=0, sticky=(tk.N, tk.E, tk.S, tk.W))
        content.columnconfigure(0, weight=3)
        content.columnconfigure(1, weight=1)
        content.rowconfigure(0, weight=1)
        content.rowconfigure(1, weight=1)
        self.root_frames['content'] = content

        # opengl frame
        opengl_frame = ttk.Frame(content, padding=2)
        opengl_frame.grid(column=0, row=0, rowspan=2, sticky=(tk.N, tk.E, tk.S, tk.W))
        self.root_frames['opengl'] = opengl_frame
        opengl_frame.rowconfigure(1, weight=1)
        opengl_frame.columnconfigure(0, weight=1)
        opengl_frame.columnconfigure(1, weight=1)

        # frames on the right
        # log_console
        log_console_frame = ttk.Frame(content, padding=2)
        log_console_frame.grid(column=1, row=1, sticky=(tk.N, tk.E, tk.S, tk.W))
        self.root_frames['log_console'] = log_console_frame
        log_console_frame.rowconfigure(1, weight=1)
        log_console_frame.columnconfigure(0, weight=1)

        # editor
        # frames on the right
        editor_frame = ttk.Frame(content, padding=2)
        editor_frame.grid(column=1, row=0, sticky=(tk.N, tk.E, tk.S, tk.W))
        self.root_frames['editor'] = editor_frame
        editor_frame.columnconfigure(0, weight=1)
        editor_frame.rowconfigure(1, weight=1)

        # prepare controls
        self.prepare_controls()

    def run(self):
        self.root.mainloop()

def main():
    frams.init()
    FramstickWindow().run()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--pybullet", action='store_true', help="Enables pybullet experimental support.")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    ENABLE_PYBULLET = args.pybullet
    main()