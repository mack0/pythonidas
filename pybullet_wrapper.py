import pybullet as p
from math import sqrt

class PybulletWrapper:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        p.connect(p.GUI)
        # create ground
        p.createCollisionShape(p.GEOM_PLANE)
        p.createMultiBody(0, 0)

        sphere_radius = .05
        self.colSphereId = p.createCollisionShape(p.GEOM_BOX, halfExtents=[sphere_radius, sphere_radius, sphere_radius])# radius=sphere_radius)

        self.secondSphere = p.createCollisionShape(p.GEOM_BOX, halfExtents=[sphere_radius, sphere_radius, sphere_radius])# radius=[sphere_radius])

        colBoxId = p.createCollisionShape(p.GEOM_BOX,
                                  halfExtents=[sphere_radius, sphere_radius, sphere_radius])

        mass = 1
        # baseOrientation = [0, 0, 0, 1]
        # basePosition = [1, 1, 1]
        # visualShapeId = -1
        # link_Masses = [1, 1]
        # linkCollisionShapeIndices = [self.colSphereId, self.secondSphere]
        # linkVisualShapeIndices = [1,2]
        # linkPositions = [[0, 0, 0.11], [0, 2, 0.22]]
        # linkOrientations = [[0, 0, 0, 1], [0, 0, 0, 1]]
        # linkInertialFramePositions = [[0, 0, 0], [0, 0, 2]]
        # linkInertialFrameOrientations = [[0, 0, 0, 1], [0, 0, 0, 1]]
        # indices = [0, 1]
        # jointTypes = [p.JOINT_REVOLUTE, p.JOINT_SPHERICAL]
        # axis = [[0, 0, 1], [0, 0, 1]]


        baseOrientation = [0, 0, 0, 1]
        basePosition = [1, 1, 1]
        visualShapeId = -1
        link_Masses = [1]
        linkCollisionShapeIndices = [self.secondSphere]
        linkVisualShapeIndices = [-1]
        linkPositions = [[0, 1, 0.11]]
        linkOrientations = [[0, 0, 0, 1]]
        linkInertialFramePositions = [[0, 0, 0]]
        linkInertialFrameOrientations = [[0, 0, 0, 1]]
        indices = [0]
        jointTypes = [p.JOINT_REVOLUTE]
        axis = [[0, 0, 1]]


        # self.groundUid = p.createCollisionShape(mass)
        self.sphereUid = p.createMultiBody(mass, self.colSphereId, visualShapeId, basePosition,
                                      baseOrientation)
        sphereUid = p.createMultiBody(mass,
                                      self.colSphereId,
                                      visualShapeId,
                                      [0, 0, 0],
                                      baseOrientation,
                                      linkMasses=link_Masses,
                                      linkCollisionShapeIndices=linkCollisionShapeIndices,
                                      linkVisualShapeIndices=linkVisualShapeIndices,
                                      linkPositions=linkPositions,
                                      linkOrientations=linkOrientations,
                                      linkInertialFramePositions=linkInertialFramePositions,
                                      linkInertialFrameOrientations=linkInertialFrameOrientations,
                                      linkParentIndices=indices,
                                      linkJointTypes=jointTypes,
                                      linkJointAxis=axis)
        p.changeDynamics(sphereUid,
                       -1,
                       spinningFriction=0.001,
                       rollingFriction=0.001,
                       linearDamping=0.0)
        for joint in range(p.getNumJoints(sphereUid)):
            p.setJointMotorControl2(sphereUid, joint, p.VELOCITY_CONTROL, targetVelocity=1, force=100)

        p.setGravity(0, 0, 0)
        p.changeDynamics(self.sphereUid,
                       -1,
                       spinningFriction=0.001,
                       rollingFriction=0.001,
                       linearDamping=0.0)

        # uncomment to run in the same thread as pythonidas
        # import time
        # while(1):
        #     spherePos, _ = p.getBasePositionAndOrientation(self.sphereUid)
        #     linear_vel, angular_vel = p.getBaseVelocity(self.sphereUid)
        #     p.applyExternalForce(self.sphereUid, -1, [0, -1, 0], spherePos, flags=p.WORLD_FRAME)
        #     # for joint in range(p.getNumJoints(sphereUid)):
        #     # p.setJointMotorControl2(sphereUid, 0, p.VELOCITY_CONTROL, targetVelocity=1, force=2)
        #     p.stepSimulation()
        #     print(linear_vel, spherePos, sqrt(linear_vel[0]**2+linear_vel[1]**2+linear_vel[2]**2))
        #     time.sleep(.01)


    def on_draw(self):
        '''
        Invoked from pythonidas gui.
        '''
        spherePos, sphereOrientation = p.getBasePositionAndOrientation(self.sphereUid)
        linear_vel, angular_vel = p.getBaseVelocity(self.sphereUid)
        p.applyExternalForce(self.sphereUid, -1, [0, -100, -10], spherePos, flags=p.WORLD_FRAME)
        print(linear_vel, spherePos, sqrt(linear_vel[0]**2+linear_vel[1]**2+linear_vel[2]**2))
        p.stepSimulation()



