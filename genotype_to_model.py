from OpenGL.GL import *
from OpenGL.GLU import gluNewQuadric
from pyopengl_models.sphere import Sphere
from pyopengl_models.stick import Stick
from pyopengl_models.cylinder import Cylinder
from pyopengl_models.cuboid import Cuboid
import math
import frams

# indices of x,y,z coordinates in the lists
DIM_X = 0
DIM_Y = 1
DIM_Z = 2

def get_dimensions(m):
    '''
    Get dimensions of the model (frams object) represented by m.
    '''
    min_dims = [m.getPart(0).p.x, m.getPart(0).p.y, m.getPart(0).p.z]
    max_dims = [*min_dims] # copy values

    for i in range(m.getPartCount()):
        dims = m.getPart(i).p.x, m.getPart(i).p.y, m.getPart(i).p.z
        for i in range(len(dims)):
            min_dims[i] = min(min_dims[i], dims[i])
            max_dims[i] = max(max_dims[i], dims[i])

    return min_dims, max_dims


def create_parts(glQuadratic, m, translate_to_position, translate_center, stick_radius = 0.1):
    parts = []
    for i in range(m.getPartCount()):
        if m.getPart(i).shape == frams.Part.Shape.SHAPE_BALL_AND_STICK:
            parts.append(Sphere(glQuadratic, stick_radius, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_ELLIPSOID:
            parts.append(Sphere(glQuadratic, 1, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_CUBOID:
            parts.append(Cuboid(glQuadratic, 1, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        elif m.getPart(i).shape == frams.Part.Shape.SHAPE_CYLINDER:
            parts.append(Cylinder(glQuadratic, 1, 2, m.getPart(i).p, translate_to_position, translate_center, m.getPart(i).o.getAngles(), m.getPart(i).scale, m.getPart(i).vcolor ))
        else:
            raise Exception("Unknown part shape: '" + str(m.getPart(i).shape) + "'")
    return parts

def create_joints(glQuadratic, m, parts, translate_to_position, translate_center, stick_radius = 0.1):
    joints = []
    for i in range(m.getJointCount()):
        if m.getJoint(i).shape == frams.Joint.Shape.SHAPE_BALL_AND_STICK:
            p1, p2 = m.getJoint(i).p1_refno, m.getJoint(i).p2_refno
            joints.append(Stick(glQuadratic, stick_radius, parts[p1].position, parts[p2].position, translate_to_position, translate_center, m.getJoint(i).vcolor ))
        elif m.getJoint(i).shape == frams.Joint.Shape.SHAPE_FIXED:
            pass
        else:
            raise Exception("Unknown joint shape: '" + str(m.getJoint(i).shape) + "'")
    return joints

def create_objects(models, camera_translation, start_x = .0, max_height = .0, distance = 2, stick_radius = .1):
    glQuadratic = gluNewQuadric()
    objects = []

    for m in models:
        min_dims, max_dims = get_dimensions(m)

        translate_to_position = (start_x - min_dims[DIM_X], -(min_dims[DIM_Y] + max_dims[DIM_Y])*1.0/2, -(min_dims[DIM_Z] + max_dims[DIM_Z])*1.0/2)
        start_x = start_x + math.sqrt((max_dims[DIM_X] - min_dims[DIM_X])**2 + (max_dims[DIM_Y] - min_dims[DIM_Y])**2) + distance
        translate_center = ((max_dims[DIM_X]+min_dims[DIM_X])/2, (max_dims[DIM_Y]+min_dims[DIM_Y])/2, (max_dims[DIM_Z]+min_dims[DIM_Z])/2)

        height = max_dims[DIM_Y]-min_dims[DIM_Y]
        if height > max_height: max_height = height

        parts = create_parts(glQuadratic, m, translate_to_position, translate_center, stick_radius=stick_radius)
        joints = create_joints(glQuadratic, m, parts, translate_to_position, translate_center, stick_radius=stick_radius)

        objects += parts
        objects += joints

    # move camera
    camera_translation[0] = (start_x-distance)/2
    camera_translation[2] = -max(camera_translation[0], 2.5*max_height)

    return objects, camera_translation


