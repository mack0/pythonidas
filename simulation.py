class Simulation:
    def __init__(self):
        self.genotypes = []

    @property
    def active_genotype(self):
        if len(self.genotypes) > 0:
            return self.genotypes[0]
        else:
            return None

    def add_genotype(self, genotype):
        self.genotypes.append(genotype)

    def remove_all_genotypes(self):
        self.genotypes.clear()