from OpenGL.GL import *
from OpenGL.GLU import gluDisk, gluCylinder
import pyglet

import math

class Cylinder(object):
    def __init__(self, glQuadratic, radius, length, position, translate_to_position, translate_to_center, rotation, scale, color):
        self.glQuadratic = glQuadratic
        self.position = position
        self.radius = radius
        self.length = length
        self.color = color
        self.scale= scale
        self.translate_to_position = translate_to_position
        self.translate_to_center = translate_to_center

        radians_to_degrees = 180.0/math.pi

        self.angle_x = rotation.x * radians_to_degrees
        self.angle_y = rotation.y * radians_to_degrees
        self.angle_z = rotation.z * radians_to_degrees

    def draw(self, rotation):
        glColor3f(self.color.x, self.color.y, self.color.z)
        glPushMatrix()

        glTranslatef(self.translate_to_position[0], self.translate_to_position[1], self.translate_to_position[2])
        glTranslatef(self.translate_to_center[0], self.translate_to_center[1], self.translate_to_center[2])
        glRotatef(rotation[0], rotation[1], rotation[2], rotation[3])
        glTranslatef(-self.translate_to_center[0], -self.translate_to_center[1], -self.translate_to_center[2])

        glTranslatef(self.position.x, self.position.y, self.position.z)
        glRotatef(90.0, 0, 1, 0)
        glRotatef(-self.angle_z, 1, 0, 0)
        glRotatef(-self.angle_y, 0, 1, 0)
        glRotatef(self.angle_x, 0, 0, 1)
        glTranslatef(0, 0, -self.length/2.0*self.scale.x)
        glScalef(self.scale.z, self.scale.y, self.scale.x)

        glPushMatrix()
        glRotatef(180, 0, 1, 0)
        gluDisk(self.glQuadratic, 0, self.radius, 30, 2)
        glPopMatrix()
        gluCylinder(self.glQuadratic, self.radius, self.radius, self.length, 25, 25)
        glPushMatrix()
        glTranslatef(0, 0, self.length)
        gluDisk(self.glQuadratic, 0, self.radius, 30, 2)
        glPopMatrix()

        glPopMatrix()

