FIND_PACKAGE(PythonInterp REQUIRED)
FIND_PACKAGE(PythonLibs REQUIRED)

SET(Boost_PYTHON2_MIN_VERSION 1.49)
SET(Boost_PYTHON3_MIN_VERSION 1.55)

IF(${PYTHON_VERSION_MAJOR} LESS 3)
	FIND_PACKAGE(Boost ${Boost_PYTHON2_MIN_VERSION} REQUIRED COMPONENTS python)
ELSE()
	SET(version ${PYTHON_VERSION_STRING})

	STRING(REGEX REPLACE "[^0-9]" "" boost_py_version ${version})
	FIND_PACKAGE(Boost ${Boost_PYTHON3_MIN_VERSION} COMPONENTS "python-py${boost_py_version}")
	SET(Boost_PYTHON_FOUND ${Boost_PYTHON-PY${boost_py_version}_FOUND})

	WHILE(NOT "${version}" STREQUAL "" AND NOT Boost_PYTHON_FOUND)
		STRING(REGEX REPLACE "([0-9.]+).[0-9]+" "\\1" version ${version})

		STRING( REGEX REPLACE "[^0-9]" "" boost_py_version ${version} )
		FIND_PACKAGE(Boost ${Boost_PYTHON3_MIN_VERSION} COMPONENTS "python-py${boost_py_version}")
		SET(Boost_PYTHON_FOUND ${Boost_PYTHON-PY${boost_py_version}_FOUND})

		STRING(REGEX MATCHALL "([0-9.]+).[0-9]+" has_more_version ${version})
		IF("${has_more_version}" STREQUAL "")
			BREAK()
		ENDIF()
	ENDWHILE()

	IF(NOT Boost_PYTHON_FOUND)
		FIND_PACKAGE(Boost ${Boost_PYTHON3_MIN_VERSION} REQUIRED COMPONENTS python3)
	ENDIF()
ENDIF()

IF(NOT FRAMSTICKS_SDK_ROOT)
	SET(FRAMSTICKS_SDK_ROOT ${PROJECT_SOURCE_DIR}/..)
ENDIF()

FIND_LIBRARY(FRAMSTICKS_SDK_LIBRARY "libFramsticksSDK${CMAKE_STATIC_LIBRARY_SUFFIX}"
	${FRAMSTICKS_SDK_ROOT}
	${FRAMSTICKS_SDK_ROOT}/cpp
)

IF(FRAMSTICKS_SDK_LIBRARY)
	IF(EXISTS ${FRAMSTICKS_SDK_LIBRARY})
		MESSAGE(STATUS "Found FramsticksSDKLibrary: ${FRAMSTICKS_SDK_LIBRARY}")
	ELSE()
		MESSAGE(FATAL_ERROR "Could not find FramsticksSDKLibrary. Check if the library path is correct.")
	ENDIF()
ELSE()
	MESSAGE(FATAL_ERROR "Could not find FramsticksSDKLibrary. Check if the root directory is correct.")
ENDIF()
