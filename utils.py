import os
import time
import tkinter as tk
from tkinter import ttk
from tkinter.constants import RIGHT, LEFT, Y, BOTH

WARNING='warning'
ERROR='error'
INFO='info'

def read_file(filename):
    """
    Returns a tuple. (True, String) if read file successfully,
    where String is the content of that file.
    (False, None) otherwise.
    """
    if os.path.exists(filename):
        with open(filename, 'r') as f:
            return True, ''.join(f.readlines())
    return False, None

def write_file(filename, content):
    if os.path.exists(os.path.dirname(filename)):
        with open(filename, 'w') as f:
            f.write(content)
            return True
    return False

def get_log_time():
    return time.strftime("%d-%m-%Y %H-%M-%S")

class ScrolledText(tk.Text):
    '''
    ScrolledText implementation copied from the original ScrolledText.
    Added textvariable to handle binded text.
    '''
    def __init__(self, master=None, textvariable = None, **kw):
        self.frame = ttk.Frame(master)
        self.vbar = ttk.Scrollbar(self.frame)
        self.vbar.pack(side=RIGHT, fill=Y)

        kw.update({'yscrollcommand': self.vbar.set})
        tk.Text.__init__(self, self.frame, **kw)
        self.pack(side=LEFT, fill=BOTH, expand=True)
        self.vbar['command'] = self.yview

        if textvariable is not None:
            if not isinstance(textvariable, tk.Variable):
                raise TypeError( "tkinter.Variable type expected, {} given.".format( type( textvariable ) ) )
            self.textvariable = textvariable
            self.textvariable.get = self.get_text
            self.textvariable.set = self.set_text

        # Copy geometry methods of self.frame without overriding Text
        # methods -- hack!
        text_meths = vars(tk.Text).keys()
        methods = vars(tk.Pack).keys() | vars(tk.Grid).keys() | vars(tk.Place).keys()
        methods = methods.difference(text_meths)

        for m in methods:
            if m[0] != '_' and m != 'config' and m != 'configure':
                setattr(self, m, getattr(self.frame, m))

    def clear(self):
        self.delete(1.0, tk.END)

    def get_text(self):
        text = self.get(1.0, tk.END)
        if text is not None and text != '':
            return text.strip()
        return None

    def set_text(self, value):
        self.clear()
        if value is not None:
            self.insert(tk.END, value.strip())

    def __str__(self):
        return str(self.frame)