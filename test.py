from __future__ import print_function
import frams
import sys

def partShapeToString(shape):
	if shape == frams.Part.Shape.SHAPE_DEFAULT:
		return "SHAPE_DEFAULT"
	elif shape == frams.Part.Shape.SHAPE_ELLIPSOID:
		return "SHAPE_ELLIPSOID"
	elif shape == frams.Part.Shape.SHAPE_CUBOID:
		return "SHAPE_CUBOID"
	elif shape == frams.Part.Shape.SHAPE_CYLINDER:
		return "SHAPE_CYLINDER"
	else:
		return "UNKNOWN"

def jointShapeToString(shape):
	if shape == frams.Joint.Shape.SHAPE_DEFAULT:
		return "SHAPE_DEFAULT"
	elif shape == frams.Joint.Shape.SHAPE_SOLID:
		return "SHAPE_SOLID"
	else:
		return "UNKNOWN"

def main(argv):
	if len(argv) < 2:
		print("Usage: python test.py [genotype]")
		sys.exit(1)

	frams.init()

	genoStr = argv[1]

	g = frams.Geno(genoStr)
	if g.isValid() == False:
		print("Invalid genotype")
		sys.exit(1)
	m = frams.Model(g)

	print("size:", m.size.x, m.size.y, m.size.z)

	print("parts count:", m.getPartCount())
	for i in range(m.getPartCount()):
		print("part", m.getPart(i).refno)
		print("\t", "3d coordinates:")
		print("\t", "\t", m.getPart(i).p.x, m.getPart(i).p.y, m.getPart(i).p.z)
		print("\t", "rotation 3x3 matrix:")
		print("\t", "\t", m.getPart(i).o.x.x, m.getPart(i).o.x.y, m.getPart(i).o.x.z)
		print("\t", "\t", m.getPart(i).o.y.x, m.getPart(i).o.y.y, m.getPart(i).o.y.z)
		print("\t", "\t", m.getPart(i).o.z.x, m.getPart(i).o.z.y, m.getPart(i).o.z.z)
		print("\t", "rot:")
		print("\t", "\t", m.getPart(i).o.getAngles().x, m.getPart(i).o.getAngles().y, m.getPart(i).o.getAngles().z)
		print("\t", "rot:")
		print("\t", "\t", m.getPart(i).rot.x, m.getPart(i).rot.y, m.getPart(i).rot.z)
		print("\t", "shape:")
		print("\t", "\t", partShapeToString(m.getPart(i).shape))
		print("\t", "size:")
		print("\t", "\t", m.getPart(i).size)
		print("\t", "scale:")
		print("\t", "\t", m.getPart(i).scale.x, m.getPart(i).scale.y, m.getPart(i).scale.z)
		print("\t", "vcolor:")
		print("\t", "\t", m.getPart(i).vcolor.x, m.getPart(i).vcolor.y, m.getPart(i).vcolor.z)
		print

	print("joints count:", m.getJointCount())
	for i in range(m.getJointCount()):
		print("joint", m.getJoint(i).refno)
		print("\t", "position delta:")
		print("\t", "\t", m.getJoint(i).d.x, m.getJoint(i).d.y, m.getJoint(i).d.z)
		print("\t", "rot:")
		print("\t", "\t", m.getJoint(i).rot.x, m.getJoint(i).rot.y, m.getJoint(i).rot.z)
		print("\t", "shape:")
		print("\t", "\t", jointShapeToString(m.getJoint(i).shape))
		print("\t", "parts' reference numbers:")
		print("\t", "\t", m.getJoint(i).p1_refno, m.getJoint(i).p2_refno)
		print("\t", "vcolor:")
		print("\t", "\t", m.getPart(i).vcolor.x, m.getPart(i).vcolor.y, m.getPart(i).vcolor.z)
		print

if __name__ == '__main__':
	exit(main(sys.argv))
