from __future__ import print_function
import frams
import sys

def main(argv):
	if len(argv) < 2:
		print("Usage: python test.py [file.gen]")
		sys.exit(1)

	frams.init()

	loader = frams.MiniGenotypeLoader(argv[1])

	l = loader.loadNextGenotype()
	while l:
		print(l.name.c_str())
		#print(l.genotype.c_str())

		l = loader.loadNextGenotype()

if __name__ == '__main__':
	exit(main(sys.argv))
