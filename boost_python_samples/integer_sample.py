from __future__ import print_function

import integer_sample as integer
import sys

def main(argv):
	x = integer.Integer(3)

	y = integer.create()

	print(x, y)

	print(x.x, y.x)

	integer.alter_ref(x)
	integer.alter_ptr(y)

	print(x, y)
	print(x.x, y.x)

	del x

	try:
		print(x)
	except Exception as e:
		print(e);

if __name__ == '__main__':
	exit(main(sys.argv))
