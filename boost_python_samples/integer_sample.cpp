#include <iostream>

struct Integer {
	Integer(int x) : x(x) {
		std::cout << "Integer::Integer(" << x << ")" << std::endl;
	}

	Integer(const Integer& i) : x(i.x) {
		std::cout << "Integer::Integer(i)" << std::endl;
	}

	~Integer() {
		std::cout << "Integer::~Integer()" << std::endl;
	}

	int x;
};

void alter_ref(Integer& a) {
	a.x += 11;
}

void alter_ptr(Integer* a) {
	a->x -= 13;
}

const Integer create(int x = -1) {
	return Integer(x);
}

#include <boost/python.hpp>

BOOST_PYTHON_FUNCTION_OVERLOADS(create_overloads, create, 0, 1)

BOOST_PYTHON_MODULE(integer_sample) {
	using namespace boost::python;

	class_<Integer>("Integer", init<int>())
		.def_readwrite("x", &Integer::x)
	;

	def("alter_ref", alter_ref);
	def("alter_ptr", alter_ptr);
	def("create", create, create_overloads());
}
