# based on: http://stackoverflow.com/a/24277852

from __future__ import print_function

import integer_sample as integer

import sys
import os
import threading

def run_sdk():
	x = integer.Integer(3)

	y = integer.create()

	print(x, y)

	print(x.x, y.x)

	integer.alter_ref(x)
	integer.alter_ptr(y)

	print(x, y)
	print(x.x, y.x)

	del x

	try:
		print(x)
	except Exception as e:
		print(e)


def main(argv):
	global captured_stdout

	stdout_fileno = sys.stdout.fileno()
	stdout_save = os.dup(stdout_fileno)
	stdout_pipe = os.pipe()
	os.dup2(stdout_pipe[1], stdout_fileno)
	os.close(stdout_pipe[1])

	captured_stdout = ''
	def drain_pipe():
		global captured_stdout
		while True:
			data = os.read(stdout_pipe[0], 1024)
			if not data:
				break
			captured_stdout += data

	t = threading.Thread(target=drain_pipe)
	t.start()

	run_sdk()

	os.close(stdout_fileno)
	t.join()

	os.close(stdout_pipe[0])
	os.dup2(stdout_save, stdout_fileno)
	os.close(stdout_save)

	print('Captured stdout:\n%s' % captured_stdout)

if __name__ == '__main__':
	exit(main(sys.argv))
