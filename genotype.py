import frams
import os
import genotype_validator as validator
import utils as u

class GenotypeObject:
    @staticmethod
    def create_from_genotype_mini_loader_object(loaded_genotype):
        '''
        Takes GenotypeMini class and wraps it with GenotypeObject.
        During GenotypeObject initialization it validates provided genotype.
        '''
        if not loaded_genotype:
            return GenotypeObject()
        genotype_obj = GenotypeObject(
            genotype = loaded_genotype.genotype.c_str(),
            name = loaded_genotype.name.c_str())
        return genotype_obj

    @staticmethod
    def create_from_genotype_string(genotype_string):
        '''
        Takes genotype and wraps it with  GenotypeObject.
        During GenotypeObject initialization it validates provided genotype.
        '''
        genotype_obj = GenotypeObject(genotype = genotype_string)
        return genotype_obj

    def __init__(self, genotype='', name='', *args, **kwargs):
        if genotype is not None:
            self.genotype_obj = frams.Geno(genotype)
            if name is not None:
                self.set_name(name)
            self.model = frams.Model(self.genotype_obj)
        else:
            self.genotype_obj = None
            self.model = None


    def is_valid(self):
        return self.model is not None and self.model.isValid()

    @property
    def name(self):
        if self.genotype_obj is not None and self.genotype_obj.getName().c_str() is not None:
            return self.genotype_obj.getName().c_str()
        else:
            return ''

    def set_name(self, name):
        if self.genotype_obj is not None:
            self.genotype_obj.setName(frams.SString(name))

    @property
    def genotype(self):
        if self.genotype_obj is not None and self.genotype_obj.getGenes().c_str() is not None:
            return f'//{ self.genotype_obj.getFormat()}\n{self.genotype_obj.getGenes().c_str()}'
        else:
            return ''

    def set_genotype(self, genotype):
        if self.genotype_obj is not None:
            self.genotype_obj.setGenes(frams.SString(genotype))
            self.model = frams.Model(self.genotype_obj)

    def save_genotype(self, filename):
        # saves only genotype without metadata (we don't parse any metadata from gen file at the moment)
        return u.write_file(filename, self.genotype)
