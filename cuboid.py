#!/usr/bin/env python

from pyglet.gl import *
import pyglet

import math

class Cuboid(object):
    def __init__(self, glQuadratic, dimension, position, translate_to_position, translate_to_center, rotation, scale, color):
        self.glQuadratic = glQuadratic
        self.dimension = dimension
        self.rotation = rotation
        self.position = position
        self.scale = scale
        self.color = color
        self.translate_to_position = translate_to_position
        self.translate_to_center = translate_to_center

        radians_to_degrees = 180.0/math.pi

        self.angle_x = rotation.x * radians_to_degrees
        self.angle_y = rotation.y * radians_to_degrees
        self.angle_z = rotation.z * radians_to_degrees

    def draw(self, rotation):
        glColor3f(self.color.x, self.color.y, self.color.z)
       
        glPushMatrix()
        glTranslatef(self.translate_to_position[0], self.translate_to_position[1], self.translate_to_position[2])
        glTranslatef(self.translate_to_center[0], self.translate_to_center[1], self.translate_to_center[2])
        glRotatef(rotation[0], rotation[1], rotation[2], rotation[3]) 
        glTranslatef(-self.translate_to_center[0], -self.translate_to_center[1], -self.translate_to_center[2])

        glTranslatef(self.position.x, self.position.y, self.position.z)
        glRotatef(90.0, 0, 1, 0) 
        glRotatef(-self.angle_z, 1, 0, 0)
        glRotatef(-self.angle_y, 0, 1, 0)
        glRotatef(self.angle_x, 0, 0, 1)
        
        glScalef(self.scale.z, self.scale.y, self.scale.x)

        #top
        glBegin(GL_QUADS)
        glNormal3f(0.0, 1.0, 0.0)
        glVertex3f(-self.dimension, self.dimension, self.dimension)
        glVertex3f(self.dimension, self.dimension, self.dimension)
        glVertex3f(self.dimension, self.dimension, -self.dimension)
        glVertex3f(-self.dimension, self.dimension, -self.dimension)
        glEnd()

        # front
        glBegin(GL_QUADS)
        glNormal3f(0.0, 0.0, 1.0)
        glVertex3f(self.dimension, -self.dimension, self.dimension)
        glVertex3f(self.dimension, self.dimension, self.dimension)
        glVertex3f(-self.dimension, self.dimension, self.dimension)
        glVertex3f(-self.dimension, -self.dimension, self.dimension)
        glEnd()

        # right
        glBegin(GL_QUADS)
        glNormal3f(1.0, 0.0, 0.0)
        glVertex3f(self.dimension, self.dimension, -self.dimension)
        glVertex3f(self.dimension, self.dimension, self.dimension)
        glVertex3f(self.dimension, -self.dimension, self.dimension)
        glVertex3f(self.dimension, -self.dimension, -self.dimension)
        glEnd()

        # left
        glBegin(GL_QUADS)
        glNormal3f(-1.0, 0.0, 0.0)
        glVertex3f(-self.dimension, -self.dimension, self.dimension)
        glVertex3f(-self.dimension, self.dimension, self.dimension)
        glVertex3f(-self.dimension, self.dimension, -self.dimension)
        glVertex3f(-self.dimension, -self.dimension, -self.dimension)
        glEnd()

        # bottom
        glBegin(GL_QUADS)
        glNormal3f(0.0, -1.0, 0.0)
        glVertex3f(self.dimension, -self.dimension, self.dimension)
        glVertex3f(-self.dimension, -self.dimension, self.dimension)
        glVertex3f(-self.dimension, -self.dimension, -self.dimension)
        glVertex3f(self.dimension, -self.dimension, -self.dimension)
        glEnd()

        # back
        glBegin(GL_QUADS)
        glNormal3f(0.0, 0.0, -1.0)
        glVertex3f(self.dimension, self.dimension, -self.dimension)
        glVertex3f(self.dimension, -self.dimension, -self.dimension)
        glVertex3f(-self.dimension, -self.dimension, -self.dimension)
        glVertex3f(-self.dimension, self.dimension, -self.dimension)
        glEnd()

        glPopMatrix()

