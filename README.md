# README #

Here are a few examples on how to use Python with Framsticks SDK.

### Requirements ###

General:

* C++11 compiler
* CMake (>=2.8.3)
* Boost.Python (>=1.49)
* Python
* Header files and a library for Python (e.g. libpython3.6-dev)
* Header files and a library for [Framsticks SDK](http://www.framsticks.com/dev/main.html) (to build the library run ```make -f frams/Makefile-SDK lib```)

## Python requirements

It is required to install pybullet and pyopengltk for GUI and pyglet for other scripts. All those dependencies are stored in the `requirements.txt` to install from it just type:
```
pip install -r requirements.txt
```


The examples should work on Linux, Mac and Windows (Cygwin).

### Building ###

Go to the project root directory.

Run
```
#!bash
cmake .
```

For the homebrew python lib to be used, the full path has to be given:
```
#!bash
cmake -DPYTHON_LIBRARY=$PYTHON_DYLIB .
```
where $PYTHON_DYLIB is the absolute path to the python `dylib` file.

If the project root directory is not in the same directory as the Framsticks SDK ```cpp/``` directory you need to specify it, e.g.:
```
#!bash
cmake -DFRAMSTICKS_SDK_ROOT=~/Framsticks/cpp .
```

You may also specify ```BOOST_ROOT``` in the same manner.

As soon as CMake has succeeded, you may run
```
#!bash
make
```

A shared module ```frams.so``` will be created in the project root directory.

Use `cmake_clean.sh` to remove all cmake-generated files.

### Running ###

#### GUI ####

Starts `Pythonidas` graphical user interface.

```
python gui.py
```
To start it with `pybullet`, type:
```
python gui.py --pybullet
```

##### GUI description

![Pythonidas GUI](img/main_window_desc.png)

Currently user interface contains 5 major parts:

 1. Simulation state
 2. Commands bar
 3. OpenGL frame
 4. Genotype editor
 5. Logs window

All functionalities are available from `commands bar` (2), but they are accessible using key shortcuts described below:

* Genotype

    *  Open genotype [*ctrl+o*]

        Opens file with *.gen* extension. If file contains many genotypes, the first one is being opened in the *genotype editor* (4). All metadata is currently discarded.

    * Save genotype [*ctrl+s*]

        Saves genotype from *genotype editor* (4) to a file with *.gen* extension.

    * Clear genotype [*F2*]

        Clears *genotype editor* (4).

* Simulation

    * Toggle simulation state [*F9*]

        Toggles simulation state - starts simulation it when it has not been yet started or stops if it is currently on. When toggling simulation state program updates genotype by copying and validating a value from *genotype editor* (4) into the current genotype.
    * Step [*F2*]

        Performs one step of a simulation. Stops simulation if it is being executed and then executes one step. (This functionality is not yet implemented)

* Log

    * Clear *logs window* (5) [*F1*]

        Clears log.

* Camera

    * Up [*up*]

        Moves camera up.

    * Down [*down*]

        Moves camera down.

    * Left [*left*]

         Moves camera to the left.

    * Right [*right*]

        Moves camera to the right.

    * Forward [*ctrl+=*]

        Zooms object in.

    * Backward [*ctrl+-*]

        Zooms object out.

    * Initial position [*ctrl+0*]

        Sets initial position of the camera.

#### test.py ####

Prints basic information about a genotype.

Running:
```
#!bash
python test.py "X(X,X)"
```
The source code of this script may serve as a base for further extensions.

#### 3d.py ####

Displays genotypes visualized in 3D.

Arguments:
```
#!bash
--genotype [GENOTYPE [GENOTYPE ...]], -g [GENOTYPE [GENOTYPE ...]]
                      genotype string
--file [FILE [FILE ...]], -f [FILE [FILE ...]]
                      file with genotype
```

Examples:
```
#!bash
python 3d.py -g "X(X,X)"
```
```
#!bash
python 3d.py -f genotypes/Diamond.gen genotypes/Spiral_plant.gen
```
```
#!bash
python 3d.py -f genotypes/walking.gen
```

Use arrows to control model's movement, '-' and '+' to zoom in/out and F5 and ESC to enter/leave full-screen mode.

#### boost_python_samples ####

The directory contains more examples on how to use the Boost.Python library.

### Python ###

The code works with Python 3 with a few additional requirements:

* Boost.Python (>=1.55)
* ```python``` must resolve to Python 3 (e.g., by using ```virtualenv```)
* Header files and a library for Python 3 (e.g. libpython3.6-dev)

Important: Boost.Python must be built against Python 3, e.g.:
```
#!bash
brew install boost-python --with-python3
```
However, it is sometimes turned on by default (e.g. libboost-python-dev).

### Authors ###

* Szymon Ulatowski, Maciej Komosiński (Framsticks SDK)
* Agnieszka Czechumska (3D graphics)
* Adam Szczepański (interoperability between Framsticks SDK and Python)
* Jakub Tomczak (upgrade to the newest version of Framsticks SDK, add GUI)
