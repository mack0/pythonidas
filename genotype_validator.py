import frams
import os
from genotype import GenotypeObject

def validate_genotype(genotype):
    '''
    Takes string representing genotype and returns genotype model
    if the input string represents the valid genotype, None otherwise.
    '''
    g = frams.Geno(genotype)
    m = frams.Model(g)
    if not m.isValid():
        return None
    return g, m


def validate_genotype_from_list(genotypes):
    data = []
    for genotype in genotypes:
        gen, model = validate_genotype(genotype)
        obj = GenotypeObject(genotype=genotype)
        if obj.is_valid():
            genotypes.append(obj)

    if len(data) < 1:
        return False, None
    return True, data

def validate_genotype_from_file(filename):
    '''
    Returns all valid genotypes read from filename.
    '''
    genotypes = []
    loader = frams.GenotypeMiniLoader(filename)
    l = loader.loadNextGenotype()
    if not os.path.exists(filename):
        return False, None
    with open(filename, 'r') as f:
        if not l:
            obj = GenotypeObject(genotype=f.read())
            if obj.is_valid():
                genotypes.append(obj)
        else:
            while l:
                gen = GenotypeObject.create_from_genotype_mini_loader_object(l)
                if gen.is_valid():
                    genotypes.append(gen)
                l = loader.loadNextGenotype()

    return True, genotypes