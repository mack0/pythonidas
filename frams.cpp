#include <frams/model/model.h>
#include <frams/model/modelparts.h>
#include <frams/genetics/preconfigured.h>
#include <frams/genetics/geno.h>
#include <frams/genetics/genman.h>
#include <frams/util/3d.h>
#include <frams/util/sstring.h>
#include <frams/_demos/genotypeloader.h>
#include <common/virtfile/stdiofile.h>

void frams_initialize() {
	static PreconfiguredGenetics genetics;
	static StdioFileSystem_autoselect stdiofilesys;
}

size_t GenManGetOperatorsCount(const GenMan& genman) {
	return genman.GetOperators().size();
}

GenoOperators* GenManGetOperatorsAt(const GenMan& genman, size_t pos) {
	return genman.GetOperators().at(pos);
}

#include <boost/python.hpp>

BOOST_PYTHON_MODULE(frams) {
	using namespace boost::python;

	def("init", frams_initialize);

	class_<SString>("SString", init<>())
		.def(init<const char*, optional<int>>())
		.def("c_str", &SString::c_str)
	;

	class_<Pt3D>("Pt3D", init<double, double, double>())
		.def_readonly("x", &Pt3D::x)
		.def_readonly("y", &Pt3D::y)
		.def_readonly("z", &Pt3D::z)
		.def("getAngles", &Pt3D::getAngles)
	;

	class_<Orient>("Orient")
		.def_readonly("x", &Orient::x)
		.def_readonly("y", &Orient::y)
		.def_readonly("z", &Orient::z)
		.def("getAngles", (Pt3D (Orient::*)() const)&Orient::getAngles)
	;

	{
		scope scopePart = class_<Part>("Part")
			.def_readonly("p", &Part::p)
			.def_readonly("o", &Part::o)
			.def_readonly("refno", &Part::refno)
			.def_readonly("rot", &Part::rot)
			.def_readonly("shape", &Part::shape)
			.def_readonly("mass", &Part::mass)
			.def_readonly("size", &Part::size)
			.def_readonly("density", &Part::density)
			.def_readonly("friction", &Part::friction)
			.def_readonly("ingest", &Part::ingest)
			.def_readonly("assim", &Part::assim)
			.def_readonly("hollow", &Part::hollow)
			.def_readonly("scale", &Part::scale)
			.def_readonly("food", &Part::food)
			.def_readonly("vcolor", &Part::vcolor)
		;

		enum_<Part::Shape>("Shape")
			.value("SHAPE_BALL_AND_STICK", Part::SHAPE_BALL_AND_STICK)
			.value("SHAPE_ELLIPSOID", Part::SHAPE_ELLIPSOID)
			.value("SHAPE_CUBOID", Part::SHAPE_CUBOID)
			.value("SHAPE_CYLINDER", Part::SHAPE_CYLINDER)
		;
	}

	{
		scope scopeJoint = class_<Joint>("Joint")
			.def_readonly("p1_refno", &Joint::p1_refno)
			.def_readonly("p2_refno", &Joint::p2_refno)
			.def_readonly("part1", &Joint::part1)
			.def_readonly("part2", &Joint::part2)
			.def_readonly("d", &Joint::d)
			.def_readonly("rot", &Joint::rot)
			.def_readonly("shape", &Joint::shape)
			.def_readonly("refno", &Joint::refno)
			.def_readonly("vcolor", &Joint::vcolor)
		;

		enum_<Joint::Shape>("Shape")
			.value("SHAPE_BALL_AND_STICK", Joint::SHAPE_BALL_AND_STICK)
			.value("SHAPE_FIXED", Joint::SHAPE_FIXED)
		;
	}

	class_<Geno>("Geno", init<optional<const char*, char, const char*, const char*>>())
		.def("isValid", &Geno::isValid)
		.def("setGenes", &Geno::setString)
		.def("getGenes", &Geno::getGenes)
		.def("setName", &Geno::setName)
		.def("getName", &Geno::getName)
		.def("getFormat", &Geno::getFormat)

	;

	class_<GenoOperators>("GenoOperators")
		.def_readonly("name", &GenoOperators::name)
	;

	// create wrappers for overloaded methods getStyle and getFullStyle
	// those wrappers defines some intermediate representations of getStyle and getFullStylefunctions
	// named getStyle1, getStyle2 ...
	// but finally, in python, we will have proper names like getStyle and getFullStyle
	// boost doesn't work with overloaded functions without this trick
	uint32_t (GenMan::*getStyle1)(const char*, int) = &GenMan::getStyle;
	uint32_t (GenMan::*getStyle2)(const char*, const Geno*, int) = &GenMan::getStyle;

	void (GenMan::*getFullStyle1)(const char *, uint32_t *) = &GenMan::getFullStyle;
	void (GenMan::*getFullStyle2)(const char *, const Geno *, uint32_t *) = &GenMan::getFullStyle;

	class_<GenMan>("GenMan")
		.def("setDefaults", &GenMan::setDefaults)
		.def("testValidity", &GenMan::testValidity)
		.def("testGenoValidity", &GenMan::testGenoValidity)
		.def("validate", &GenMan::validate)
		.def("mutate", &GenMan::mutate)
		.def("crossOver", &GenMan::crossOver)
		.def("similarity", &GenMan::similarity)
		.def("getStyle", getStyle1)
		.def("getStyle", getStyle2)
		.def("getFullStyle", getFullStyle1)
		.def("getFullStyle", getFullStyle2)
		.def("HTMLize", (std::string (GenMan::*)(const char*))&GenMan::HTMLize)
		.def("HTMLizeShort", (std::string (GenMan::*)(const char*))&GenMan::HTMLizeShort)
		.def("getSimplest", &GenMan::getSimplest)
		.def("getOpName", &GenMan::getOpName)
		.def("getOperatorsCount", GenManGetOperatorsCount)
		.def("getOperatorsAt", GenManGetOperatorsAt,
			return_value_policy<reference_existing_object>())
	;

	class_<Model>("Model", init<Geno, optional<bool>>())
		.def("isValid", &Model::isValid)
		.def_readonly("size", &Model::size)
		.def("getPartCount", &Model::getPartCount)
		.def("getPart", &Model::getPart,
			return_value_policy<reference_existing_object>())
		.def("getJointCount", &Model::getJointCount)
		.def("getJoint", &Model::getJoint,
			return_value_policy<reference_existing_object>())
	;

	class_<GenotypeMini>("GenotypeMini")
		.def_readonly("name", &GenotypeMini::name)
		.def_readonly("genotype", &GenotypeMini::genotype)
	;

	class_<GenotypeMiniLoader>("GenotypeMiniLoader", init<const char*>())
		.def("loadNextGenotype", &GenotypeMiniLoader::loadNextGenotype,
			return_value_policy<reference_existing_object>())
	;
}
